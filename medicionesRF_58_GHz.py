#!/usr/lib/ python2.7
# -*- coding: UTF-8 -*-
import serial 
import time 
import datetime as dt 
import os 
import RPi.GPIO as GPIO  
from serial import SerialException

#####################################
####### MENSAJE DE BIENVENIDO #######
#####################################

print(":: MEDICIONES RF 5.8GHZ ::") 
time.sleep(2)

#########################
####### variables #######
#########################

### CONSTANTES ###

# Posicion inicial del Switch
SW = 1

# Vueltas de switcheo
Vueltas_Switch = 5
cont_vueltas = 0

# Velocidad del puerto serial
BAUDRATE = 500000

# Frecuencia en MHz (configuracion PM)
FRECUENCIA = 70

##################
####Variables#####
##################
midiendo = 1 
DBM = '' 
temp = '' 
vuelta = 0 
RPM = 0.0 
angulo = 0 
 
sameTurn = 0 
sameAngle = 0 
rev_tiempo_anterior = time.time() 
tiempo_lectura = 0.0 
puerto = '' 

print("Definicion de variables completa...") 
time.sleep(0.5)

###################
###### setup ######
###################

# Configuracion GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM) 
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) # GPIO24 sensor Vueltas
GPIO.setup(22, GPIO.OUT) #Encendido de Encoder PIN 15
GPIO.setup(25, GPIO.OUT) #Led de "Midiendo" PIN 22
GPIO.setup(27, GPIO.OUT) #pin del switch de antenas PIN 13

# Configuracion de archivos de medicion
foldername = "/home/pi/"+"MedicionesRF/"+"mediciones_py"+"/" #directorio donde se almacenaran las muestras 
filename1 = "potencia "+dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+".csv" #archivo .CSV que se creara para almacenar datos de potencia 
#filename2 = "rpm "+dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+".csv" #archivo .CSV que se creara para almacenar datos de rpm 

if not os.path.exists(foldername):
    os.mkdir(foldername) 
f_pot = open(foldername + filename1, 'w') #funcion abre y escribe archivo de potencia
#f_rpm = open(foldername + filename2, 'w') #funcion abre y escribe archivo de rpm 

f_pot.write("time,power,rpm,turn,antena\n") #cabecera de archivo de potencia 
#f_rpm.write("gpio,rpm,time,round\n") #cabecera de archivo de rpm

# Configuracion puerto serial powermeter
puerto = [x for x in os.listdir('/dev') if x[:6]=='ttyUSB'][0] #determina puerto utilizado por powermeter
puerto = "/dev/"+puerto 
ser = serial.Serial(
	port=puerto,
        baudrate=BAUDRATE,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
        timeout=None 
) 
print("Limpiando puerto serial.") 
ser.close() 
print("Verificando puerto serial.") 
if(not ser.isOpen()):
    print("El puerto serial esta cerrado. Abriendo puerto...")
    ser.open() 
if(ser.isOpen()):
    print("Puerto serial abierto.")
 
print(":: Setup completo ::") 
time.sleep(1)



######################################
###### comienzan las mediciones ######
######################################

print(":: Medicion iniciada ::") 
time.sleep(0.5)
 
frequency_set = 0 
freq_msg = "freq " + str(FRECUENCIA) + "\r" 
freq_test_string = freq_msg + "Frequency="+str(FRECUENCIA)+"MHz\r\n"
   
try:
    # Configuracion captura de datos powermeter
    while(not frequency_set):
        print("Fijando frecuencia...\n")
        ser.flushInput() #limpia datos almacenados del powermeter
        ser.flushOutput()
        ser.write(freq_msg) #escribe comando para configurar frecuencia de operacion de powermeter
        time.sleep(0.5)
        current_freq = ''
        current_freq = ser.read(ser.inWaiting())
        print("current freq: "+ current_freq )
        if ( current_freq == freq_test_string ):
            frequency_set = 1
    print("Frecuencia fijada en " + str(FRECUENCIA) + " MHz en el PM.")
    ser.write("cpower\r") #escribe comando para ejecucion de la toma de datos en el powermeter
    print("Solicitando lecturas de potencias.")
    time.sleep(0.5)
    ser.flushInput()



    tiempo_inicio = time.time()
    while(midiendo):

	GPIO.output(22, True) #encendido del LED Encoder
	GPIO.output(25, True) #encendido del LED indicador

	# CONFIGURACION DEL SWITCHEO DE ANTENAS
	if (cont_vueltas >= Vueltas_Switch):
	    cont_vueltas = 0
	    if (SW == 1):
	        SW = 2
	        GPIO.output(27, False)
	    elif (SW == 2):
	        SW = 1
	        GPIO.output(27, True)


        # MEDICION DE RPM
        if ( (GPIO.input(24) == 1) and (sameTurn == 0) ):
            rev_tiempo_actual = time.time()
            rev_tiempo = rev_tiempo_actual - rev_tiempo_anterior
            rev_tiempo_anterior = rev_tiempo_actual
            RPM = round((1.0/rev_tiempo)*60.0,1) #realiza el calculo de RPM mediante la diferencia de tiempo entre
            #tiempo del sistema anterior y actual
            if ( angulo <> 0 ):
                angulo = 0
           # f_rpm.write("1" + ',' + str(RPM) + ',' + dt.datetime.now().strftime("%H:%M:%S.%f") + ',' + str(vuelta) + '\n')
            vuelta += 1 #aunmenta el indice "lap"
	    cont_vueltas += 1 #aumenta el contador de vueltas
            sameTurn = 1
	    print("Vuelta: " + str(vuelta))
	    print("Switch: " + str(SW))
	    print(" ")
        elif ( (GPIO.input(24) == 0) and (sameTurn == 1) ):
            sameTurn = 0
           # f_rpm.write("0" + ',' + str(RPM) + ',' + dt.datetime.now().strftime("%H:%M:%S.%f") + ',' + str(vuelta) + '\n')


        # LECTURA DEL POWERMETER
        if(ser.inWaiting()>0):
            if( (time.time()-tiempo_lectura)>0.1 ): #compara la diferencia entre del tiempo del sistema y lectura con 0.025
                ser.flushInput()
                tiempo_lectura = time.time()
		print("Vueltas: " + str(vuelta))
                DBM = '' #la variable esta vacia
            if(ser.inWaiting()>0):
                temp = ser.read(1) #lee un byte
                tiempo_lectura = time.time()
		tiempo_transcurrido = tiempo_lectura - tiempo_inicio  #Calculo de la diferencia de tiempo con el tiempo inicial
                if(temp == '\r'):
                    continue
                elif(temp == '\000'):
                    continue
                elif(temp == '\n'):
                    len_DBM = len(DBM)
                    if (len_DBM > 0): #si el largo de DBM es mayor a cero contiuna comparacion
                        if( DBM[0]=='@' ): #si el primer valor de la cadena es un @, almacena el valor de la potencia sin el indice @
                            DBM = DBM[1:len_DBM]
                            f_pot.write(str(tiempo_transcurrido) + ',' + DBM + ',' + str(RPM) + ',' + str(vuelta) + ',' + str(SW) + '\n')
                        else:
                            print("Lectura de potencia incompleta.")
                            print("se obtuvo: " + str(DBM))
                        DBM = ''
                else:
                    DBM += temp 

except KeyboardInterrupt: #genera interrupcion de teclado ctrl+c
    GPIO.output(22, False) #apaga el LED del Encoder
    GPIO.output(25, False) #apaga el LED indicador
    ser.close()
    f_pot.close()
#    f_rpm.close()
    GPIO.cleanup()
    print("Medicion finalizada por usuario.") 

except serial.SerialException: #genera excepcion en caso que exista algun evento en el puerto serial.
    GPIO.output(22, False) #apaga el LED del Encoder
    GPIO.output(25, False) #apaga el LED indicador
    ser.close()
    f_pot.close()
#    f_rpm.close()
    GPIO.cleanup()
    print("Fallo en conexion con puerto serial.")
